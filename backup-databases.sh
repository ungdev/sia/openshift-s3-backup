#!/bin/bash -e

if [[ -n "$DEBUG" && "$DEBUG" == "true" ]]
then
  set -x
fi

DATE=$(date +%Y-%m-%d_%H-%M)
DIR="/tmp/backup"

#BACKUP_HOST=""
#BUCKET=""

function log() {
  echo "$(date '+%Y-%m-%d %H:%M:%S') $1"
}

if [ -z "$AWS_ACCESS_KEY_ID" ]
then
  log "AWS_ACCESS_KEY_ID not set"
  exit 1
fi
if [ -z "$AWS_SECRET_ACCESS_KEY" ]
then
  log "AWS_SECRET_ACCESS_KEY not set"
  exit 1
fi
if [ -z "$BACKUP_HOST" ]
then
  log "BACKUP_HOST not set"
  exit 1
fi
if [ -z "$BUCKET" ]
then
  log "BUCKET not set"
  exit 1
fi
if [ -z "$RESTIC_PASSWORD" ]
then
  log "RESTIC_PASSWORD not set"
  exit 1
fi

if [ -z "$NAMESPACE_KINDS_BACKUP" ]
then
  log "NAMESPACE_KINDS_BACKUP not set"
  exit 1
fi

if [ -z "$GLOBAL_KINDS_BACKUP" ]
then
  log "GLOBAL_KINDS_BACKUP not set"
  exit 1
fi

if [ -z "$RESTIC_REPOSITORY_VERSION" ]
then
  log "RESTIC_REPOSITORY_VERSION not set"
  exit 1
fi


updateRepositoryToV2 ()
{
  chemin=$1
  resource="/${BUCKET}/${chemin}"
  log "Migrating $BACKUP_HOST/${BUCKET}/$chemin to v2"
  restic -r s3:$BACKUP_HOST/${BUCKET}/$chemin migrate upgrade_repo_v2
  restic -r s3:$BACKUP_HOST/${BUCKET}/$chemin prune --repack-uncompressed
}

putBackup ()
{
  file=$1
  chemin=$2
  resource="/${BUCKET}/${chemin}"

  if [[ -n "$RESTIC_UPDATE_V2" && "$RESTIC_UPDATE_V2" == "true" ]]
  then
    updateRepositoryToV2 $chemin
  fi
  log "Init the repository if not exists"
  restic -r s3:$BACKUP_HOST/${BUCKET}/$chemin init --repository-version $RESTIC_REPOSITORY_VERSION
  log "Unlock stale locks"
  restic -r s3:$BACKUP_HOST/${BUCKET}/$chemin unlock
  #checks the backups if specified in environment variable
  if [[ -n "$RESTIC_HAS_TO_CHECK" && "$RESTIC_HAS_TO_CHECK" == "true" ]]
  then
    log "Check backup integrity"
    restic -r s3:$BACKUP_HOST/${BUCKET}/$chemin check --with-cache
  fi
  log "Start the backup"
  restic -r s3:$BACKUP_HOST/${BUCKET}/$chemin backup $BACKUP_OPTIONS $file
  log "Prune the old backups"
  restic -q -r s3:$BACKUP_HOST/${BUCKET}/$chemin forget --prune --keep-within 2d --keep-daily 14 --keep-weekly 6 --keep-monthly 100
}


mkdir $DIR
for j in dc sts deploy
do
  for i in mysql postgresql postgres mongodb mariadb mongodb-bitnami
  do
    case $j in
      dc)
        WORKLOADS=$(oc get $j -l backup=$i --all-namespaces -o jsonpath='{range .items[?(@.status.availableReplicas > 0)]}{.metadata.name}{" "}{.metadata.namespace}{"\n"}{end}')
        ;;
      deploy)
        WORKLOADS=$(oc get $j -l backup=$i --all-namespaces -o jsonpath='{range .items[?(@.status.availableReplicas > 0)]}{.metadata.name}{" "}{.metadata.namespace}{"\n"}{end}')
        ;;
      sts)
        WORKLOADS=$(oc get $j -l backup=$i --all-namespaces -o jsonpath='{range .items[?(@.status.readyReplicas > 0)]}{.metadata.name}{" "}{.metadata.namespace}{"\n"}{end}')
        ;;
    esac
    if [ ! -z "$WORKLOADS" ]
    then
      while read DC_NAME PROJECT
      do
        case $j in
          sts)
            POD=$DC_NAME"-0"
            ;;
          dc)
            POD=$(oc get pods -n $PROJECT -l deploymentconfig=$DC_NAME  -o jsonpath='{range .items[?(@.status.phase == "Running")]}{.metadata.name}{end}' | head -n 1)
            ;;
        esac
        log "$PROJECT $DC_NAME $POD"
        DBNAME=""
        case $i in
          mysql)
            DBNAME=$(oc -n $PROJECT exec $POD -- /bin/bash -c 'echo $MYSQL_DATABASE')
            if [ ! -z "$DBNAME" ]
            then
              log "Backup database $DBNAME..."
              oc -n $PROJECT exec $POD -- /bin/bash -c 'mysqldump -h 127.0.0.1 -u $MYSQL_USER --password=$MYSQL_PASSWORD $MYSQL_DATABASE' > $DIR/$PROJECT-$DBNAME.sql
              putBackup $DIR/$PROJECT-$DBNAME.sql mysql/$PROJECT/$DBNAME
            fi
            ;;
          mariadb)
            DBNAME=$(oc -n $PROJECT exec $POD -- /bin/bash -c 'echo $MARIADB_DATABASE')
            if [ ! -z "$DBNAME" ]
            then
              log "Backup database $DBNAME..."
              oc -n $PROJECT exec $POD -- /bin/bash -c 'mysqldump -h 127.0.0.1 -u $MARIADB_USER --password=$MARIADB_PASSWORD $MARIADB_DATABASE' > $DIR/$PROJECT-$DBNAME.sql
              putBackup $DIR/$PROJECT-$DBNAME.sql mysql/$PROJECT/$DBNAME
            fi
            ;;
          postgresql)
            DBNAME=$(oc -n $PROJECT exec $POD -- /bin/bash -c 'echo $POSTGRESQL_DATABASE')
            if [ ! -z "$DBNAME" ]
            then
              log "Backup database $DBNAME..."
              oc -n $PROJECT exec $POD -- /bin/bash -c 'pg_dump -Fc $POSTGRESQL_DATABASE' > $DIR/$PROJECT-$DBNAME.pg
              putBackup $DIR/$PROJECT-$DBNAME.pg pgsql/$PROJECT/$DBNAME
            fi
            ;;
          postgres)
            DBNAME=$(oc -n $PROJECT exec $POD -- /bin/bash -c 'echo $POSTGRES_DB')
            if [ ! -z "$DBNAME" ]
            then
              log "Backup database $DBNAME..."
              oc -n $PROJECT exec $POD -- /bin/bash -c 'pg_dump -Fc --dbname=postgresql://$POSTGRES_USER:$POSTGRES_PASSWORD@127.0.0.1:5432/$POSTGRES_DB' > $DIR/$PROJECT-$DBNAME.pg
              putBackup $DIR/$PROJECT-$DBNAME.pg pgsql/$PROJECT/$DBNAME
            fi
            ;;
          mongodb)
            DBNAME=$(oc -n $PROJECT exec $POD -- /bin/bash -c 'echo $MONGODB_DATABASE')
            if [ ! -z "$DBNAME" ]
            then
              log "Backup database $DBNAME..."
              oc -n $PROJECT exec $POD -- /bin/bash -c 'mongodump -u $MONGODB_USER -p $MONGODB_PASSWORD -d $MONGODB_DATABASE' > $DIR/$PROJECT-$DBNAME.mongodump
              putBackup $DIR/$PROJECT-$DBNAME.mongodump mongodb/$PROJECT/$DBNAME
            fi
            ;;
          mongodb-bitnami)
            DBNAME=$(oc -n $PROJECT exec $POD -- /bin/bash -c 'echo $MONGODB_DATABASE')
            if [ ! -z "$DBNAME" ]
            then
              log "Backup database $DBNAME..."
              oc -n $PROJECT exec $POD -- /bin/bash -c 'mongodump -u $MONGODB_USERNAME -p $MONGODB_PASSWORD -d $MONGODB_DATABASE' > $DIR/$PROJECT-$DBNAME.mongodump
              putBackup $DIR/$PROJECT-$DBNAME.mongodump mongodb/$PROJECT/$DBNAME
            fi
            ;;
          *)
            log "ERROR: Unknown backup-method $i"
            ;;
        esac
      done <<< $WORKLOADS
    fi
  done
done

log "Backuping namespace ressources $NAMESPACE_KINDS_BACKUP..."

NAMESPACES=$(oc get namespaces -l backup=true -o jsonpath='{range .items[*]}{.metadata.name}{" "}{end}')
for namespace in $NAMESPACES
do
  ADDITIONNAL_KINDS=$(oc get ns/$namespace -o jsonpath='{.metadata.annotations.additionnalKindsBackup}')
  KINDS="$NAMESPACE_KINDS_BACKUP $ADDITIONNAL_KINDS"
  log "       Backuping $namespace with additionnal kinds $ADDITIONNAL_KINDS..."
  for kind in $KINDS
  do
    RESSOURCES=$(oc get $kind -n $namespace -o jsonpath='{range .items[*]}{.metadata.name}{" "}{end}')
    mkdir -p /tmp/yaml/$namespace/$kind
    for ressource in $RESSOURCES
    do
      oc get $kind/$ressource -n $namespace -o yaml > /tmp/yaml/$namespace/$kind/$ressource.yaml
    done
  done

  putBackup /tmp/yaml/$namespace yaml/$namespace
done


log "Backuping global ressources $GLOBAL_KINDS_BACKUP"
for kind in $GLOBAL_KINDS_BACKUP
do
  RESSOURCES=$(oc get $kind -o jsonpath='{range .items[*]}{.metadata.name}{" "}{end}')
  mkdir -p /tmp/yaml-cluster/$kind
  for ressource in $RESSOURCES
  do
    oc get $kind/$ressource -o yaml > /tmp/yaml-cluster/$kind/$ressource.yaml
  done
done
putBackup /tmp/yaml-cluster yaml-cluster
